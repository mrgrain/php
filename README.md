# prefernz/php #

Customized version of official [php images](https://hub.docker.com/r/_/php/) with build in support for Composer and some extensions. 

### Customizations ###

#### Packages ####
We add the following commandline tools:

- git
- ssh
- curl
- zip
- unzip

#### PHP ####
We are adding support for Composer, MySQL services and internationalization.
The default error reporting is more verbose.

The added extensions are in detail:

- mcrypt (only in 7.1)
- pdo_mysql
- mysqli
- intl
- gd
- bcmath

#### Apache 2 ####
We are moving the document root to `/var/www/public` and enable `mod_rewrite` by default.


### Who do I talk to? ###

* @mrgrain
